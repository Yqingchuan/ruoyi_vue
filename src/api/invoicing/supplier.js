import request from '@/utils/request'

// 查询供应商列表
export function listSup(query) {
  return request({
    url: '/invoicing/supplier/find',
    method: 'get',
    params: query
  })
}



// 新增供应商
export function addSup(data) {
  return request({
    url: '/invoicing/supplier',
    method: 'post',
    data: data
  })
}

// 修改供应商
export function updateSup(data) {
  return request({
    url: '/invoicing/supplier',
    method: 'put',
    data: data
  })
}
// 主键查询供应商详细
export function getSup(supId) {
  return request({
    url: '/invoicing/supplier/' + supId,
    method: 'get'
  })
}

// 删除工厂
export function delSup(supId) {
  return request({
    url: '/invoicing/supplier/' + supId,
    method: 'delete'
  })
}
