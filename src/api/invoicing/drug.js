import request from '@/utils/request'

// 查询药品列表
export function listDrug(query) {
  return request({
    url: '/invoicing/drug/find',
    method: 'get',
    params: query
  })
}
//查询所有生产厂家
export function listFacName(){
  return request({
    url:'/invoicing/drug/findF',
    method: 'get'
  })
}
//查询处方列表
export function listPres(query) {
  return request({
    url: '/invoicing/drug/findP',
    method: 'get'
  })
}
//查询药品类型列表
export function listDrugType(query) {
  return request({
    url: '/invoicing/drug/findD',
    method: 'get'
  })
}


// 新增药品
export function addDrug(data) {
  return request({
    url: '/invoicing/drug',
    method: 'post',
    data: data
  })
}

// 修改药品
export function updateDrug(data) {
  return request({
    url: '/invoicing/drug',
    method: 'put',
    data: data
  })
}
// 查询药品详细
export function getDrug(drugId) {
  return request({
    url: '/invoicing/drug/' + drugId,
// 删除药品

    method: 'get'
  })
}

// 删除药品
export function delDrug(drugId) {
  return request({
    url: '/invoicing/drug/' + drugId,
    method: 'delete'
  })
}
