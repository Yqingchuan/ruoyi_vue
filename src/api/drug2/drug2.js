import request from '@/utils/request'

// 查询科室列表
export function listBill(query) {
  return request({
    url: '/system/drug/list',
    method: 'get',
    params: query
  })
}

export function getGys() {
  return request({
    url: '/system/supperlier/name', //
    method: 'get'
  })
}

// 查询部门列表（排除节点）
export function listDeptExcludeChild(deptId) {
  return request({
    url: '/system/dept/list/exclude/' + deptId,
    method: 'get'
  })
}

// 查询部门详细
export function getDept(deptId) {
  return request({
    url: '/system/hisdept/' + deptId,
    method: 'get'
  })
}

// 查询部门下拉树结构
export function treeselect() {
  return request({
    url: '/system/dept/treeselect',
    method: 'get'
  })
}

// 根据角色ID查询部门树结构
export function roleDeptTreeselect(roleId) {
  return request({
    url: '/system/dept/roleDeptTreeselect/' + roleId,
    method: 'get'
  })
}

// 新增科室
export function addDept(data) {
  return request({
    url: '/system/hisdept',
    method: 'post',
    data: data
  })
}

// 修改部门
export function updateDept(data) {
  return request({
    url: '/system/hisdept',
    method: 'put',
    data: data
  })
}

// 删除部门
export function delbill(billId) {
  return request({
    url: '/system/drug/' + billId,
    method: 'delete'
  })

}
//更新
export function updatebill(billId) {
  return request({
    url: '/system/drug/' + billId,
    method: 'put'
  })
}

export function updatebill2(billId2) {
  return request({
    url: '/system/supperlier/' + billId2,
    method: 'put'
  })
}
