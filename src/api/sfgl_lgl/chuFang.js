import request from '@/utils/request'

// 根据订单编号查询患者信息以及检查处方表和药品处方表
export function listPost(query) {
  return request({
    url: 'sfgl_lgl/recipe/search',
    method: 'get',
    params: query
  })
}
//生成订单
export function PayPost(data) {
  return request({
    url: '/sfgl_lgl/recipe/pay',
    method: 'post',
    params:data,
    //raditional: true,//这里设为true就可以了
  })
}
//根据挂号单号、患者姓名、支付状态动态查询订单信息
export function listSfTable(query) {
  return request({
    url: 'sfgl_lgl/search/index',
    method: 'get',
    params: query
  })
}

// 退费
export function refund(ddh) {
  return request({
    url: '/sfgl_lgl/search/refund',
    method: 'get',
    params:ddh
  })
}
