import request from '@/utils/request'

// 查询角色列表
export function listRole(query) {
  return request({
    url: '/visits/lists/list',
    method: 'get',
    params: query
  })
}


// 新增角色
export function getRole(s_ids) {
  return request({
    url: '/visits/lists/editd?s_ids='+s_ids,
    method: 'get',
  })
}

// 修改角色
export function getPost(s_ids) {
  return request({
    url: '/visits/lists/edit?s_ids='+s_ids,
    method: 'get',
  })
}

// 角色数据权限
export function dataScope(data) {
  return request({
    url: '/system/role/dataScope',
    method: 'put',
    data: data
  })
}

// 角色状态修改
export function changeRoleStatus(roleId, status) {
  const data = {
    roleId,
    status
  }
  return request({
    url: '/system/role/changeStatus',
    method: 'put',
    data: data
  })
}

// 删除角色
export function delRole(roleId) {
  return request({
    url: '/system/role/' + roleId,
    method: 'delete'
  })
}

// 导出角色
export function exportRole(query) {
  return request({
    url: '/system/role/export',
    method: 'get',
    params: query
  })
}
