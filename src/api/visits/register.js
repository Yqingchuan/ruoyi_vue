import request from '@/utils/request'

// 查询岗位列表
export function listPost(query) {
  return request({
    url: '/visits/register/list',
    method: 'get',
    params: query
  })
}

// 查询角色详细
export function getPost(s_idnumber) {
  return request({
    url: '/visits/register/list?s_idnumber=' + s_idnumber,
    method: 'get'
  })
}

// 新增岗位
export function addPost(data) {
  return request({
    url: '/system/post',
    method: 'post',
    data: data
  })
}

// 修改岗位
export function updatePost(data) {
  return request({
    url: '/visits/lists',
    method: 'put',
    data: data
  })
}

// 删除岗位
export function delPost(postId) {
  return request({
    url: '/system/post/' + postId,
    method: 'delete'
  })
}

// 导出岗位
export function exportPost(query) {
  return request({
    url: '/system/post/export',
    method: 'get',
    params: query
  })
}
