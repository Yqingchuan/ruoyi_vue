import request from '@/utils/request'

// 查询岗位列表
export function listPost(query) {
  return request({
    url: '/kbjz_lss/ghlb/list',
    method: 'get',
    params: query
  })
}

// 查询岗位详细
export function getPost(postId) {
  return request({
    url: '/system/post/' + postId,
    method: 'get'
  })
}

// 新增岗位
export function addPost(data) {
  return request({
    url: '/system/post',
    method: 'post',
    data: data
  })
}

// 修改挂号单，患者状态
export function updatePost(data) {
  return request({
    url: '/kbjz_lss/ghlb',
    method: 'put',
    data: data
  })
}

// 删除挂号单
export function delPost(regId) {
  return request({
    url: '/kbjz_lss/ghlb/' + regId,
    method: 'delete'
  })
}

// 导出岗位
export function exportPost(query) {
  return request({
    url: '/system/post/export',
    method: 'get',
    params: query
  })
}
