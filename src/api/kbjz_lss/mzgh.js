import request from '@/utils/request'


// 根据身份证号，查询患者信息
export function getPost(patiCode) {
  return request({
    url: '/kbjz_lss/hzk/' + patiCode,
    method: 'get'
  })
}
// 查询挂号单列表
export function listPost(query) {
  return request({
    url: '/kbjz_lss/ghlb/list',
    method: 'get',
    params: query
  })
}
//新增挂号表
export function addReg(data) {
  return request({
    url: '/kbjz_lss/ghlb/',
    method: 'post',
    data: data
  })
}
// 新增岗位
export function addPost(data) {
  return request({
    url: '/kbjz_lss/hzk/',
    method: 'post',
    data: data
  })
}

// 修改岗位
export function updatePost(data) {
  return request({
    url: '/system/post',
    method: 'put',
    data: data
  })
}

// 删除岗位
export function delPost(postId) {
  return request({
    url: '/system/post/' + postId,
    method: 'delete'
  })
}

// 导出岗位
export function exportPost(query) {
  return request({
    url: '/system/post/export',
    method: 'get',
    params: query
  })
}
